body {
font-size: 95%;
}

main {
background-color: #ffffff !important;
background-image: none;
}

table {
border-collapse:collapse;

font-size: 90%;
font-family:Muli,Tahoma,Verdana,"DejaVu Sans",sans-serif;
}

tr:nth-of-type(odd) {
background:#fff !important;
}

tr:nth-of-type(even) {
background:#fff !important;
}


th {
font-weight:normal;
background-color:#fff !important;
padding: 10px !important;
border-bottom: 0px solid #E9E9E9 !important;
border-top: 0px solid #E9E9E9 !important;
}

td,th {
padding:.5em;
text-align:left;
}

tr {
border-bottom:0px solid #fff
}

td,th {
border-right:0px solid #fff
}

header {
background-color: #FFFFFF;
position: relative;
width: 100%;
}

.region-highlighted {

}

#logoImage {
    height: 275px !important;
}

header #header-custom-height-2ndlevel {
    height: 140px !important;
}

.front-page-banner {
    max-height: 340px !important;
  }


.site-name {
display:none;
}

.parent-site-header {
  height: 34px;
  background-color: #0083d6;
  width: 100%;
  font-size: 110%;
  font-weight: bold;
}

.parent-site-header a {
  color: #ffffff;
  display: block;
  -webkit-transition: 0.1s;
  -moz-transition: 0.1s;
  -o-transition: 0.1s;
  transition: 0.1s;
}

.parent-site-header a:hover, .parent-site-header a:active, .parent-site-header a:focus, .parent-site-header a:visited {
  -webkit-transition: 0.5s;
  -moz-transition: 0.5s;
  -o-transition: 0.5s;
  transition: 0.5s;
  text-decoration: none;
  color: #ffffff;
}


#onesearch {
background-color: #3283C4;
}

header#department {
display: none;
}

.site-header {
}

.home-cal {
margin-left: 15px;
width: 300px;
}

#search-box-standalone {
position: absolute;
top: 70px;
left: 225px;
background-color: rgba(0, 131, 214, 0.6); 
font-size: 0.8em; 
padding:0px; 
width: 50%;
margin-left: auto;
margin-right: auto;
border: 0px solid rgba(0, 0, 0, 0.5);
-moz-border-radius: 10px;
-webkit-border-radius: 10px;
border-radius: 4px;
max-width: 1000px;
min-width: 650px;
line-height: 28px;
}



#inside-search-box {
background-color: rgba(0, 0, 0, 0.7);
}

input.rounded {
border: 0px solid #ccc;
-moz-border-radius: 10px;
-webkit-border-radius: 10px;
border-radius: 4px;
-moz-box-shadow: 2px 2px 3px #666;
-webkit-box-shadow: 2px 2px 3px #666;
box-shadow: 2px 2px 3px #666;
font-size: 13px;
color: #666666;
padding: 4px 4px;
margin-left: 15px;
outline: 0;
-webkit-appearance: none;
width: 300px;
}

input.rounded:focus {
border-color: #808080;
}

select.rounded {
border: 0px solid #ccc;
-moz-border-radius: 10px;
-webkit-border-radius: 10px;
border-radius: 4px;
-moz-box-shadow: 2px 2px 3px #666;
-webkit-box-shadow: 2px 2px 3px #666;
box-shadow: 2px 2px 3px #666;
font-size: 13px;
color: #8E8E8C;
padding: 3px 3px;
margin-left: 0px;
outline: 0;
height: 24px;
-webkit-appearance: none;
-moz-appearance: none;
background: url(http://library.wwu.edu/sites/library.wwu.edu/files/images/dropdown_arrow_on.gif) no-repeat right #fff;
}

select.rounded option.room {
font-size: 13px;
padding-top: 4px !important;
}


input.roundedbutton {
border: 1px solid #fff;
-moz-border-radius: 10px;
-webkit-border-radius: 10px;
border-radius: 4px;
-moz-box-shadow: 2px 2px 3px #666;
-webkit-box-shadow: 2px 2px 3px #666;
box-shadow: 2px 2px 3px #666;
font-size: 14px;
padding: 2px 5px;
margin-left: 1px;
outline: 0;
-webkit-appearance: none;
background: #bad80a;
color: #003f87;
}

input.roundedbutton:focus {
border-color: #808080;
}


input.roundedbutton:hover {
background: #ffffff;
color: #003f87;
border-color: #003f87;
}

.onesearch-links {
padding: 17px;
color: #ffffff;
font-weight: 600;
}

.mywesternlinks {
margin: 20px 0px 90px 5px;
}

.onesearch-links-new {
padding: 0px 0px 15px 20px;
color: #ffffff;
font-weight: bold;
}

.onesearch-links a:link, .onesearch-links a:visited {
color: #ffffff;
text-decoration: none;
}
.onesearch-links a:hover, .onesearch-links a:active {
text-decoration: underline;
}

.onesearch-links-new a:link, .onesearch-links-new a:visited {
color: #ffffff;
text-decoration: none;
}
.onesearch-links-new a:hover, .onesearch-links-new a:active {
text-decoration: underline;
}

#api_search_group_iid572 select {
width: 250px !important;
}


.tagline {
position: relative; 
left: 70px; 

font-weight: normal;
}

.cal-link {
position: relative;
top: -25px;
left: 40px;
}

.home-chat {
margin-top: -20px;
}

div.home-chat span {
color: #ffffff; 
font-size: 14px; 
font-weight: bold;
float: right;
padding-right: 10px;
}

.home-chat {
float: right;
padding: 40px 40px 0px 0px;
}

.set-height {
min-height:395px;
max-height:395px;
}

.today {
margin-left: -20px;
width: 100%;
position: absolute;
float: right;
top: 5px;
font-size: 100%;
color: #fff;

}

.today a {
color: #fff;
text-decoration: underline;
}

footer a {
color: #ffffff;
text-align: left;
}

#panel-home-4col {
border: 0px #003f87 solid;
max-height: 800px;
min-height: 400px;
padding: 5px;


}

#servicesfor {
height: 460px;
}

#acct-access {
height: 460px;
}

#acct-access li {
margin-left: -10px;
}

#newsevents {
height: 460px;
}

#newsevents li {
margin-left: -10px;
}

#newsevents ul {
border: 0px solid #E9E9E9 !important;
background-color: #fff;
}

#eventscal {
height: 460px;
}

#servicesfor li, #acct-access li, #newsevents li{
padding-top: 10px;
}

#panel-home-4col .panel-panel {
border: 4px #E9E9E9;
border-style:solid; 
min-height: 400px;
max-height: 600px;
margin-bottom: -5px !important;
}


#panel-home-4col .pane-title {
background-color: #bad80a;
padding: 5px 5px;

font-size: 95%;
font-weight: bold;
margin-top: 0px;
margin-left: 0px;
width: 95%;
}

#panel-home-4col .more-link {
float: right;
font-size: small;
padding-top: 10px;
padding-right: 10px;
}

#libcal{
margin: 20px;
}

.showtight {
float: right;
font-size: small;
margin-top: -15px;
}



#api_upc_cid1721_iid388 #th3, .ttitles, .tdes, .tcat {
display: none;
}

#api_upc_cid1721_iid388 td {
padding-bottom: 0px !important;
padding-top: 0px !important;
}

#api_upc_cid1721_iid388 td a {
padding-top: 0px !important;
}

#api_upc_cid1721_iid388 tr {
background-color: rgba(0, 0, 0, 0);
border-bottom: #ececec 0px dotted !important;
line-height: 19px;
}


/* Events*/
.tevent {
@import "http://fonts.googleapis.com/css?family=Muli:400,400italic";
font-family: Muli,Tahoma,Verdana,"DejaVu Sans",sans-serif !important;
font-size: 90% !important;
width: 100% !important; 
border-bottom: 1px dotted #ccc !important;
border-collapse: separate !important;
border-spacing: 1px !important;
margin-bottom: 0.5em !important;
margin-top: 0.7em !important;
}

.hrmenu {
border: 0; height: 1px; width: 75%;background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0));
}

.tevent {
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 0.7em !important;
    margin-top: 0.7em !important;
}

#landing-page .panel-pane, .panelizer-view-mode .panel-pane {
border: 4px 4px 0px 4px !important;
border-color: #E9E9E9 !important;
border-style:solid !important;
padding: 10px !important;
margin-bottom: 10px !important;
}

#landing-page .panel-pane, .panelizer-view-mode .panel-pane h2 {
padding-top: 0px !important;
}

.pane-entity-field, .pane-node-links.link-wrapper {
border: 0px;
}

.pane-block-36 {

}

.view-id-cpnws .view-footer a {

}

.view-id-cpnws .view-footer li, .view-id-giving_page .view-footer li {
margin-left: 30px !important;
font-size: 0.925em;
}

#title10063557.headerbox, #title2463523.headerbox, #title9366115.headerbox, #title10881926.headerbox, #title11048036.headerbox, #profile_links, #api_profile_uid89788_iid572 .headerbox, #api_profile_uid14753_iid572 .headerbox, #api_boxes_iid92_boxmoreways .la_headerbox, #la_qid_443257 .la_topics { 
  display: none !important; 
}

.outerbox {
  padding: 0px !important;
}

#api_box_iid572_bid10881926 {
background-color: #000000 !important;
}

.jonroom {
margin-top: 30px;
}

.jonroom2 {
margin-top: -30px;
}

.pane-block h2, .pane-views h2 {
    font-size: 20px;
    margin-top: 0.5em;
}

.pane-block h2 img, .pane-views h2 img {
    vertical-align: middle;
}

.footer-left {
margin-left: 3%;
}

.footer-left #footer-left-heading {
background-color:#fff;width:100%;height:2.520em;-webkit-box-shadow:#555 0 0 7px;-moz-box-shadow:#555 0 0 7px;box-shadow:#555 0 0 7px; 
}

.footer-left #footer-left-heading h3 {
font-size:1em;color:#003f87;margin-top:-25px;padding:0.9em;border:none;text-transform:uppercase
}

.footer-left #footer-left-contact-info {
padding-right:1.25em;
margin-top:1.500em;
font-size:.750em
}

.footer-left p:first-child, .footer-left dl:first-child {
font-size:95%;
line-height: 1.7;
margin-bottom:1.500em;
display: block;
clear: both !important;
}



.footer-left #footer-left-contact-info img {
float:left;padding-right:.625em;
}

.footer-left #footer-left-contact-info p {
margin-bottom:10;
}


.footer-left #footer-left-contact-info a {
color:#fff
}

.footer-right {
    margin-left: 64.5%; 
}


.view-id-library_exhibits .view-display-id-page_4 img {
    float: left;
    margin: 5px 5px 20px 10px;
    padding: 0;

}

.view-id-library_exhibits td {
    border-bottom: 1px dotted #333333;
    width: 50%;
    vertical-align: top;
}

.view-id-library_exhibits .col-1 {
    border-right: 1px dotted #333333;
}

.view-id-library_exhibits img {
    margin-top: 5px; 
}

.view-id-library_exhibits li {
display: block; list-style: none; text-decoration: none;
}

.site-header .site-banner {
    max-height: 170px;
    overflow: hidden;
}

.page-title {
margin-bottom: .5em;
}

.page-title h1 {
display: none; 
}

.footer-center {
margin-left: 30%;  
width: 25%;
}

#online-chat {
float: right; 
margin-top:4px;
padding-right: 60px;
}

.pane-news article {
border-bottom: 3px solid #E9E9E9;
padding: 10px 0px;
}

#newsview .field-name-taxonomy-vocabulary-6 {
display: none;
}