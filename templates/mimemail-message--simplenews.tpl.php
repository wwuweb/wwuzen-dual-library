<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */

?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
#new3things
{ }

#main_3things {
  border-top: 0;
  border-bottom: 0;
  border-style: solid;
  border-width: 5px;
  border-collapse: collapse;
  border-color: #ececec;
  margin-left: 0px;
  margin-right: 0px;
  width: 810px;
}

.threethings-introtext {
  font-family: Verdana,"DejaVu Sans",sans-serif;
  font-size: 0.9em;
  line-height: 1.5em;
  margin: 15px;
  word-wrap: break-word;
  text-align: justify;
}

.threethings-text {
  font-family: Verdana,"DejaVu Sans",sans-serif;
  font-size: 0.9em;
  line-height: 1.5em;
  margin: 15px;
  word-wrap: break-word;
  text-align: justify;
}

.threethings-text img {
  margin-left: 15px;
}

.threethings-links p {
  line-height: 1.5em;
  margin-left: 10px;
  margin-bottom: 10px;
  margin-top: 5px;
  font-family: Verdana,"DejaVu Sans",sans-serif; font-size: .9em;
}

.threethings-links a {
  color: #003e87;
  text-underline: none;
}

.threethings-links a:hover {
  color: #003e87;
}

#footer_3things {
  border-top: 0;
  border-bottom: 0;
  border-style: solid;
  border-width: 0px;
  border-collapse: collapse;
  border-color: #ececec;
  margin-left: 0px;
  margin-right: 0px;
  width: 810px;
}
</style>

  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>>
    <div id="center">
      <div id="main">
        <?php print $body ?>
      </div>
    </div>
  </body>
</html>
